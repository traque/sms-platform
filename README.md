# sms-platform

Plateforme d'envoi de SMS pour la Traque, marchant via MAXS, une appli de
contrôle à distance du téléphone via XMPP.

## Mise en place

### Prérequis

Tout d'abord, il est **nécessaire** d'avoir un serveur XMPP en place. En
particulier, MAXS demande que les records DNS SRV soient en place pour se
connecter facilement. Il est également nécessaire d'avoir de la crypto sur le
serveur XMPP. Ne pas oublier que techniquement, quiconque arrive à rentrer sur
ce serveur prend le contrôle des téléphones des orgas.

Sur ce serveur, un compte XMPP doit être créé pour le serveur, par exemple
`serveur@example.com`. Un compte doit être également créé par téléphone.

Il est également nécessaire d'avoir un certain nombre de téléphones sous
Android, sur lesquels on installera l'application "MAXS" avec le transport XMPP
et le module `smssend`. Configurer les téléphones pour qu'ils se connectent à
XMPP, et qu'ils acceptent les commandes de `serveur@example.com`.

### Installation

L'installation du site lui-même se fait comme d'habitude :

```bash
git clone https://git.eleves.ens.fr/traque/sms-platform
cd sms-platform
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
cp traque_sms_platform/settings{.TYPE,}.py  # TYPE = dev or prod
$EDITOR traque_sms_platform/settings.py     # Change required/desired settings
./manage.py migrate
```

### Données à entrer

* Créer un compte par organisateurice qui doit être capable d'envoyer des SMS.
  Donner le statut `staff` à cette personne.
* Créer une entrée `SmsDevice` par téléphone organisateur.
* Créer une entrée `SmsRecipient` par personne à qui envoyer des SMS.
* Créer une entrée `SmsRecipientGroup` par groupe auquel vous souhaiterez
  envoyer des SMS. Ces groupes peuvent être récursifs.
