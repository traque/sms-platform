""" Production settings template for Traque-SMS-Platform """

from .settings_base import *
import os

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ""  # FIXME

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []  # FIXME

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {  # FIXME
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = "fr-fr"
TIME_ZONE = "Europe/Paris"
USE_I18N = True
USE_L10N = True
USE_TZ = True

# XMPP settings
XMPP_JID = ""  # FIXME
XMPP_PASSWORD = ""  # FIXME
