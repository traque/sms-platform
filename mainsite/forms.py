from django import forms
from sms_send.models import SmsGroupMessage


class SendSmsForm(forms.ModelForm):
    """ Form to send a SMS """

    class Meta:
        model = SmsGroupMessage
        fields = ["recipients", "body"]
        labels = {"recipients": "Envoyer à", "body": "Message"}
