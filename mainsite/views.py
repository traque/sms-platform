import itertools

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView, SingleObjectMixin
from django.views.generic.edit import CreateView
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.db.models import Max

from sms_send.models import (
    SmsMessage,
    SmsGroupMessage,
    SmsDevice,
    SmsRecipient,
    SmsRecipientGroup,
)
from .forms import SendSmsForm


class IsStaffMixin(PermissionRequiredMixin):
    permission_required = "is_staff"


class HomeView(IsStaffMixin, TemplateView):
    """ Home page """

    template_name = "mainsite/home.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["sms_sent"] = SmsMessage.objects.filter(sent_status="S").count()
        context["sms_errors"] = SmsMessage.objects.filter(sent_status="E").count()
        return context


class SendSmsView(IsStaffMixin, CreateView):
    """ Send a SMS """

    template_name = "mainsite/send_sms.html"

    model = SmsGroupMessage
    # fields = ["recipients", "body"]
    form_class = SendSmsForm

    def get_success_url(self):
        return reverse(
            "mainsite:prev_sms_status_details", kwargs={"pk": self.object.pk}
        )

    def form_valid(self, form):
        self.object = form.save()
        self.object.instanciate()
        self.object.send_all()
        return HttpResponseRedirect(self.get_success_url())


class SmsResendView(IsStaffMixin, SingleObjectMixin, RedirectView):
    model = SmsGroupMessage
    http_method_names = ["post"]

    def get_redirect_url(self, *args, **kwargs):
        self.object = self.get_object()
        self.object.send_all()
        return reverse(
            "mainsite:prev_sms_status_details", kwargs={"pk": self.object.pk}
        )


class ReassignDevicesView(IsStaffMixin, RedirectView):
    http_method_names = ["post"]

    @staticmethod
    def round_robin(l_a, l_b):
        """ Iterates over the elements of a, with elements of b in a round-robin
        fashion.
        Eg. if l_a = [1, 2, 3, 4, 5], l_b = [1, 2], this will yield
        [(1,1), (2,2), (3,1), (4,2), (5,1)].
        """
        for (a, b) in itertools.zip_longest(l_a, itertools.cycle(l_b)):
            if a:  # After iterating over l_a, a will be None
                yield (a, b)
            else:
                break

    def get_redirect_url(self, *args, **kwargs):
        recipients = SmsRecipient.objects.all()
        devices = SmsDevice.objects.all()
        devices = list(filter(lambda x: x.is_available(), devices))
        if not devices:
            messages.error(
                self.request,
                (
                    "Impossible de réassigner les destinataires : "
                    + "aucun téléphone d'envoi disponible actuellement."
                ),
            )
        else:
            for (recipient, device) in self.round_robin(recipients, devices):
                recipient.last_sms_device = device
                recipient.save()
        return reverse("mainsite:devices_status")


class PreviousSmsDetailsView(IsStaffMixin, DetailView):
    model = SmsGroupMessage
    template_name = "mainsite/previous_sms_details.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["group_message"] = self.object
        context["individual_messages"] = self.object.individual_messages.all()
        return context


class PreviousSmsListView(IsStaffMixin, ListView):
    model = SmsGroupMessage
    template_name = "mainsite/previous_sms_list.html"

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.prefetch_related("individual_messages").annotate(
            max_date=Max("individual_messages__sent_date")
        )


class DevicesStatusView(IsStaffMixin, TemplateView):
    template_name = "mainsite/devices_status.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["devices"] = SmsDevice.objects.all().prefetch_related(
            "smsrecipient_set"
        )
        return context
