from django.urls import path
from .views import (
    HomeView,
    SendSmsView,
    SmsResendView,
    PreviousSmsDetailsView,
    PreviousSmsListView,
    DevicesStatusView,
    ReassignDevicesView,
)

app_name = "mainsite"

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("send_sms/", SendSmsView.as_view(), name="send_sms"),
    path(
        "prev_sms/<int:pk>/",
        PreviousSmsDetailsView.as_view(),
        name="prev_sms_status_details",
    ),
    path("prev_sms/", PreviousSmsListView.as_view(), name="prev_sms_status"),
    path("devices_status/", DevicesStatusView.as_view(), name="devices_status"),
    path("sms_resend/<int:pk>/", SmsResendView.as_view(), name="sms_resend"),
    path("reassign_devices/", ReassignDevicesView.as_view(), name="reassign_devices"),
]
