# Generated by Django 2.2.5 on 2019-09-11 09:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sms_send', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SmsRecipientGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Human-readable label', max_length=512)),
                ('group_recipients', models.ManyToManyField(blank=True, help_text='Whole groups belonging to this group', related_name='metagroups', to='sms_send.SmsRecipientGroup')),
                ('individual_recipients', models.ManyToManyField(blank=True, help_text='Individual SmsRecipients belonging to this group', related_name='group_memberships', to='sms_send.SmsRecipient')),
            ],
        ),
        migrations.CreateModel(
            name='SmsGroupMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('body', models.TextField()),
                ('recipients', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sms_send.SmsRecipientGroup')),
            ],
        ),
        migrations.AddField(
            model_name='smsmessage',
            name='group_message',
            field=models.ForeignKey(default=None, help_text='Group message this message is an instance of', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='individual_messages', to='sms_send.SmsGroupMessage'),
        ),
    ]
