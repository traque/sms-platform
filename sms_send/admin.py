from django.contrib import admin
from .models import (
    SmsDevice,
    SmsRecipient,
    SmsRecipientGroup,
    SmsMessage,
    SmsGroupMessage,
)


admin.site.register(SmsDevice)
admin.site.register(SmsRecipient)
admin.site.register(SmsRecipientGroup)
admin.site.register(SmsMessage)
admin.site.register(SmsGroupMessage)
