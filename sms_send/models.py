import logging
from django.db import models
from django.core import validators
from django.utils import timezone
from .maxs_util import MaxsDevice

logger = logging.getLogger(__name__)


class SmsDevice(models.Model):
    """ Physical device capable of sending SMSs """

    device_name = models.CharField(
        help_text="Human-readable device name", max_length=255
    )
    device_xmpp_account = models.CharField(
        unique=True, help_text="XMPP account used to control this phone", max_length=255
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.device_handler = MaxsDevice.get_instance(self.device_xmpp_account)

    def is_available(self):
        return self.device_handler.is_available()

    def __str__(self):
        return "{} <{}> ({})".format(
            self.device_name,
            self.device_xmpp_account,
            "UP" if self.is_available() else "DOWN",
        )


class SmsRecipient(models.Model):
    """ A recipient of SMS messages """

    last_sms_device = models.ForeignKey(
        SmsDevice,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        default=None,
        help_text="SMS device last used to send SMS to this recipient",
    )
    phone_number = models.CharField(
        validators=[validators.RegexValidator(regex=r"\+[0-9]+")],
        unique=True,
        max_length=20,
    )
    name = models.CharField(
        help_text="Human-readable name for this recipient", max_length=255
    )

    @property
    def device(self):
        """ Get this recipient's sending device """
        if self.last_sms_device and self.last_sms_device.is_available():
            return self.last_sms_device
        possible_devices = (
            SmsDevice.objects.all()
            .annotate(num_recipients=models.Count("smsrecipient"))
            .order_by("num_recipients")
        )
        for cur_device in possible_devices:
            if cur_device.is_available():
                self.last_sms_device = cur_device
                self.save()
                return cur_device
        return None

    def __str__(self):
        return "{} <{}>".format(self.name, self.phone_number)


class SmsRecipientGroup(models.Model):
    """ A group of recipients, possibly recursive """

    name = models.CharField(max_length=512, help_text="Human-readable label")
    individual_recipients = models.ManyToManyField(
        SmsRecipient,
        blank=True,
        related_name="group_memberships",
        help_text="Individual SmsRecipients belonging to this group",
    )
    group_recipients = models.ManyToManyField(
        "SmsRecipientGroup",
        blank=True,
        related_name="metagroups",
        help_text="Whole groups belonging to this group",
    )

    def all_recipients(self):
        """ Recursively aggregate individual recipients into a queryset """
        qs = self.individual_recipients.all()
        for group in self.group_recipients.all():
            qs = qs.union(group.all_recipients())
        return qs

    def __str__(self):
        return "{} ({} individuals, {} groups)".format(
            self.name, self.individual_recipients.count(), self.group_recipients.count()
        )


class SmsMessage(models.Model):
    SENT_STATUS_CHOICES = [
        ("N", "Never sent"),
        ("P", "Pending"),
        ("S", "Sent"),
        ("E", "Error while sending"),
    ]

    class AlreadySent(Exception):
        def __str__(self):
            return "This message has already been sent"

    recipient = models.ForeignKey(SmsRecipient, on_delete=models.CASCADE)
    body = models.TextField()
    sent_status = models.CharField(
        max_length=2, choices=SENT_STATUS_CHOICES, default="N"
    )
    sent_date = models.DateTimeField(
        null=True,
        default=None,
        help_text="Time and date at which this message was sent",
    )
    group_message = models.ForeignKey(
        "SmsGroupMessage",
        on_delete=models.CASCADE,
        related_name="individual_messages",
        null=True,
        blank=True,
        default=None,
        help_text="Group message this message is an instance of",
    )
    sending_device = models.ForeignKey(
        SmsDevice,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
        help_text="Device who sent — or tried to send — this message",
    )

    @classmethod
    def sent_status_to_text(cls, status):
        return dict(cls.SENT_STATUS_CHOICES)[status]

    @property
    def sent_status_text(self):
        return self.sent_status_to_text(self.sent_status)

    def __str__(self):
        return "To {}: {} [{}]".format(
            self.recipient.name, self.body[:20] + "…", self.sent_status_text
        )

    def send(self, bypass_already_sent=False):
        if not self.pk:
            raise Exception("This message must be saved first.")

        if not bypass_already_sent and self.sent_status in ["S", "P"]:
            raise self.AlreadySent()

        device = self.recipient.device
        if not device:
            self.update_status("E")
            logger.error("Cannot send {}: no available device".format(self.pk))
            return
        self.sending_device = device
        self.save()
        device.device_handler.send_sms(self.recipient.phone_number, self.body, self)
        self.update_status("P")

    def update_status(self, status):
        self.sent_status = status
        self.sent_date = timezone.now()
        self.save()

    def mark_sent(self):
        self.update_status("S")


class SmsGroupMessage(models.Model):
    recipients = models.ForeignKey(SmsRecipientGroup, on_delete=models.CASCADE)
    body = models.TextField()

    def instanciate(self):
        """ Create all the individual messages """
        if not self.pk:
            raise Exception("SmsGroupMessage must be saved before instanciating")
        for recipient in self.recipients.all_recipients():
            message = SmsMessage(
                recipient=recipient, body=self.body, group_message=self
            )
            message.save()

    @property
    def group_proeminent_status(self):
        if self.individual_messages.filter(sent_status="E").exists():
            return "E"
        elif self.individual_messages.filter(sent_status="P").exists():
            return "P"
        elif self.individual_messages.filter(sent_status="N").exists():
            return "N"
        return "S"

    @property
    def group_proeminent_status_text(self):
        return SmsMessage.sent_status_to_text(self.group_proeminent_status)

    @property
    def can_resend(self):
        return self.group_proeminent_status in ["E", "N", "P"]

    def send_all(self):
        for message in self.individual_messages.all():
            try:
                message.send()
            except SmsMessage.AlreadySent:
                # Ignore messages already sent to allow resending after errors occurred
                pass
