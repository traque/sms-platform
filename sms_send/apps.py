from django.apps import AppConfig


class SmsSendConfig(AppConfig):
    name = 'sms_send'
