from .models import SmsDevice


def devices_availability(request):
    """ Adds information about the availability of SMS devices """
    devices = SmsDevice.objects.all()
    total_devices = devices.count()
    available_devices = 0
    for device in devices:
        if device.is_available():
            available_devices += 1

    available_ratio = available_devices / total_devices if total_devices > 0 else 0
    status = "ok"
    if available_devices == 0:
        status = "critical"
    elif available_ratio < 0.5:
        status = "error"
    elif available_devices < total_devices:
        status = "warning"

    return {
        "sms_devices": devices,
        "available_sms_devices_count": available_devices,
        "sms_devices_count": total_devices,
        "sms_devices_status": status,
    }
