""" Communicates with a device using MAXS """

from datetime import timedelta
import logging
import threading
import time
import asyncio
import re
from slixmpp import ClientXMPP
from slixmpp.jid import JID
from django.conf import settings
from django.utils import timezone

logger = logging.getLogger(__name__)


class XMPPError(Exception):
    """ Triggered when some error occurred at the XMPP level """

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "An XMPP error occurred: {}".format(self.msg)


class XMPPConnection(ClientXMPP):
    """ XMPP client connection """

    sms_ack_re = re.compile(r"^Sending SMS to (?P<phone>\+?[0-9]+): (?P<body>.*)$")

    def __init__(self, do_init_xmpp=True):
        self.devices = {}
        if do_init_xmpp:
            self.init_xmpp()

    def init_xmpp(self):
        jid = settings.XMPP_JID
        password = settings.XMPP_PASSWORD
        super().__init__(jid, password)

        self.add_event_handler("session_start", self.session_start)
        self.add_event_handler("connection_failed", self.connection_failed)
        self.add_event_handler("failed_auth", self.failed_auth)
        self.add_event_handler("disconnected", self.disconnected)
        self.add_event_handler("message", self.recv_message)
        self.add_event_handler("message_error", self.recv_message_error)
        self.add_event_handler("got_online", self.recv_online)
        self.add_event_handler("got_offline", self.recv_offline)

    def add_device(self, device, device_jid):
        """ Add a managed device """
        self.devices[JID(device_jid).bare] = device

    def get_device(self, device_jid):
        return self.devices.get(JID(device_jid).bare)

    # ========== Handlers below ==========

    def session_start(self, event):
        logger.info("Connected to XMPP server")
        self.send_presence()
        self.get_roster()

    def recv_presence_update(self, presence, is_online):
        exp = presence["from"]
        device = self.get_device(exp)

        logger.info("Got presence for {} [{}]: {}".format(device, exp, is_online))

        if device:
            device.set_available(is_online)
        else:
            logger.warn("Device {} not found in {}".format(exp, self.devices))

    def recv_online(self, presence):
        self.recv_presence_update(presence, True)

    def recv_offline(self, presence):
        self.recv_presence_update(presence, False)

    def recv_message(self, msg):
        device = self.get_device(msg["from"])
        if device:
            body = msg["body"]
            ack_match = self.sms_ack_re.match(body)
            if ack_match:  # SMS sending ACK
                logger.info("SMS ack")
                device.get_sent_notif(ack_match.group("phone"), ack_match.group("body"))
        else:
            logger.warn("Device {} not found in {}".format(msg["from"], self.devices))

        logger.info(
            "Got message <{exp}> -> <{to}> [{body}]\nRaw:: {raw}".format(
                raw=msg, exp=msg["from"], to=msg["to"], body=msg["body"]
            )
        )  # TODO

    def recv_message_error(self, msg):
        logger.error("XMPP error message: {}".format(msg["body"]))

    def connection_failed(self, data):
        """ Triggered when connection to the server failed """
        raise XMPPError("connection to the server failed: {}".format(data))

    def failed_auth(self, *args, **kwargs):
        """ Triggered when auth failed """
        raise XMPPError("connection to the server failed: bad credentials")

    def disconnected(self, *args, **kwargs):
        """ Triggered when disconnected from server """
        self.connect()  # Reconnect


class ThreadedXMPPConnection(threading.Thread, XMPPConnection):
    """ Thread encapsulating the XMPP client """

    def __init__(self):
        threading.Thread.__init__(self)
        XMPPConnection.__init__(self, do_init_xmpp=False)
        self.loop = None

    def run(self):
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.init_xmpp()
        self.connect()
        while True:
            try:
                self.process()
            except Exception as exn:
                logger.error("Caught exception at base level: {}".format(exn))

    def stop(self):
        self.disconnect()
        self.loop.stop()


class XmppClientConnectMixin:
    xmpp_client = None

    def __new__(cls, *args, **kwargs):
        inst = super().__new__(cls)
        if not cls.xmpp_client:
            cls.xmpp_client = ThreadedXMPPConnection()
            cls.xmpp_client.start()
        return inst


class PendingMessage:
    def __init__(self, phone_number, body, obj=None):
        self.phone_number = phone_number.strip()
        self.body = body.strip()
        self.obj = obj

    def __eq__(self, other):
        """ Compare ignoring self.obj """
        return self.phone_number == other.phone_number and self.body == other.body


class MaxsDevice(XmppClientConnectMixin):
    """ Communicate with MAXS.

    Connects to `self.get_device_xmpp_account()`, which defaults to
    `self.device_xmpp_account`.
    """

    instances = {}

    def __init__(self, device_xmpp):
        if device_xmpp in MaxsDevice.instances:
            raise XMPPError("Device {} already instanciated.".format(device_xmpp))

        super().__init__()
        self.device_xmpp = device_xmpp
        self.last_sent = timezone.now()
        self.available = False
        self.xmpp_client.add_device(self, self.device_xmpp)
        self.pending_messages = []  # FIXME improve performance?

        logger.info("Spawning MAXS device for {}".format(self.device_xmpp))
        MaxsDevice.instances[self.device_xmpp] = self

    @classmethod
    def get_instance(cls, xmpp):
        if xmpp in cls.instances:
            return cls.instances[xmpp]
        return cls(xmpp)

    def set_available(self, available):
        """ Set the availableness of this device """
        self.available = available

    def is_available(self):
        return self.available

    def send_sms(self, to, msg, msg_obj=None):
        while timezone.now() - self.last_sent < timedelta(microseconds=5e5):
            time.sleep(0.5)
        self.last_sent = timezone.now()
        self.xmpp_client.send_message(
            self.device_xmpp, "sms send {to}  {msg}".format(to=to, msg=msg)
        )
        self.pending_messages.append(PendingMessage(to, msg, msg_obj))

    def get_sent_notif(self, phone_num, body):
        notified_msg = PendingMessage(phone_num, body)
        pending_msg = None
        pending_index = None
        logger.info("Got sent notif for <{}> <{}>".format(phone_num, body))
        for index, cur_msg in enumerate(self.pending_messages):
            if cur_msg == notified_msg:
                pending_msg = cur_msg
                pending_index = index
                logger.info("Got pending message")
                break
        if pending_msg:
            if pending_msg.obj:
                cur_msg.obj.mark_sent()
                logger.info("Pending message marked sent")
            self.pending_messages.pop(pending_index)
